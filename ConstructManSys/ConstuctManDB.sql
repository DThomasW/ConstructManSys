--use master;

--create database ConstructManDB;
--go

--use ConstructManDB;
--go

create schema Manager;
go

create table Manager.Person
(
  ManagerID int not null identity(1,1),
  FirstName nvarchar(50) not null,
  LastName nvarchar(50) not null,
  UserName nvarchar(50) not null,
  Pass nvarchar(50) not null
)

create table Manager.Project
(
  ProjectID int not null identity(1,1),
  ProjectName nvarchar(50) not null,
  StartDate datetime2(0) not null,
  EndDate datetime2(0) null,
  StatusID int not null,
  ManagerID int not null
)

create table Manager.ProjectStatus
(
  StatusID int not null identity(1,1),
  [Status] nvarchar(50) not null
)

create table Manager.Worker
(
  WorkerID int not null identity(1,1),
  FirstName nvarchar(50) not null,
  LastName nvarchar(50) not null,
  TitleID int not null,
  ProjectID int not null,
  StatusID int not null
)

create table Manager.WorkTitle
(
  TitleID int not null identity(1,1),
  TitleName nvarchar(50) not null
)

create table Manager.WorkStatus
(
  StatusID int not null identity(1,1),
  [Status] nvarchar(50) not null
)

create table Manager.Material
(
  MaterialID int not null identity(1,1),
  MaterialName nvarchar(50) not null,
  QuantityNeed int not null,
  QuantityHave int not null,
  ProjectID int not null
)
go

alter table Manager.Person add constraint pk_ManPersonID primary key clustered (ManagerID);
alter table Manager.Project add constraint pk_ManProjectID primary key clustered (ProjectID);
alter table Manager.ProjectStatus add constraint pk_ManProjStatusID primary key clustered (StatusID);
alter table Manager.Worker add constraint pk_ManWorkerID primary key clustered (WorkerID);
alter table Manager.WorkTitle add constraint pk_ManTitleID primary key clustered (TitleID);
alter table Manager.WorkStatus add constraint pk_ManWorkStatusID primary key clustered (StatusID);
alter table Manager.Material add constraint pk_ManMaterialID primary key clustered (MaterialID);
go

alter table Manager.Project alter column StatusID int not null;

alter table Manager.Project add constraint fk_ManProjectStatus foreign key (StatusID) references Manager.ProjectStatus(StatusID);
alter table Manager.Project add constraint fk_ManProjectManager foreign key (ManagerID) references Manager.Person(ManagerID);
alter table Manager.Worker add constraint fk_ManWorkerTitle foreign key (TitleID) references Manager.WorkTitle(TitleID);
alter table Manager.Worker add constraint fk_ManWorkerStatus foreign key (StatusID) references Manager.WorkStatus(StatusID);
alter table Manager.Worker add constraint fk_ManWorkerProject foreign key (ProjectID) references Manager.Project(ProjectID);
alter table Manager.Material add constraint fk_ManMaterialProj foreign key (ProjectID) references Manager.Project(ProjectID);
go

insert into Manager.Person(FirstName, LastName, UserName, Pass) values ('D',  'T', 'Dev1', '1234');
go

insert into Manager.ProjectStatus([Status]) values ('InProgress');
insert into Manager.ProjectStatus([Status]) values ('Delayed');
insert into Manager.ProjectStatus([Status]) values ('Completed');
insert into Manager.ProjectStatus([Status]) values ('Cancelled');
go

insert into Manager.WorkTitle(TitleName) values ('Supervisor');
insert into Manager.WorkTitle(TitleName) values ('Trainor');
insert into Manager.WorkTitle(TitleName) values ('Worker');
insert into Manager.WorkTitle(TitleName) values ('HR');
insert into Manager.WorkTitle(TitleName) values ('Designer');
insert into Manager.WorkTitle(TitleName) values ('Lawyer');
insert into Manager.WorkTitle(TitleName) values ('Security');
go


insert into Manager.WorkStatus([Status]) values ('Training');
insert into Manager.WorkStatus([Status]) values ('Working');
insert into Manager.WorkStatus([Status]) values ('Sick');
insert into Manager.WorkStatus([Status]) values ('Vacation');
insert into Manager.WorkStatus([Status]) values ('Terminated');
go

select * from Manager.Person;
go

create proc Manager.sp_updateManager @mID int, @fName nvarchar(50), @lName nvarchar(50), @user nvarchar(50), @pass nvarchar(50) as
  update Manager.Person set FirstName = @fName, LastName = @lName, UserName= @user, Pass = @pass
  where ManagerID = @mID;
 go

 create proc Manager.sp_updateProject @pID int, @name nvarchar(50), @start datetime2(0), @end datetime2(0), @status int, @man int as
  update Manager.Project set ProjectName = @name, StartDate = @start, EndDate = @end, StatusID = @status, ManagerID = @man
  where ProjectID = @pID;
 go

 create proc Manager.sp_updateWorker @wID int, @fName nvarchar(50), @lName nvarchar(50), @status int, @title int, @project int as
  update Manager.Worker set FirstName = @fName, LastName = @lName, StatusID= @status, TitleID = @title, ProjectID = @project
  where WorkerID = @wID;
 go

 create proc Manager.sp_updateMaterial @mID int, @name nvarchar(50), @need int, @have int, @project int as
  update Manager.Material set MaterialName = @name, QuantityHave = @have, QuantityNeed= @need, ProjectID = @project
  where MaterialID = @mID;
 go

 create proc Manager.sp_addManager @fName nvarchar(50), @lName nvarchar(50), @user nvarchar(50), @pass nvarchar(50) as
  insert into Manager.Person(FirstName, LastName, UserName, Pass) values (@fName, @lName, @user, @pass)
  go
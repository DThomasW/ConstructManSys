﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ConstructService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICMSService" in both code and config file together.
    [ServiceContract]
    public interface ICMSService
    {
        [OperationContract]
        List<ManagerDAO> GetAllManagers();

        [OperationContract]
        ManagerDAO GetManager(int id);

        [OperationContract]
        ManagerDAO GetManager(string first, string last);

        [OperationContract]
        ManagerDAO GetManagerByLogin(string user, string pass);

        [OperationContract]
        ManagerDAO MakeManager(string first, string last, string user, string pass);

        [OperationContract]
        ManagerDAO UpdateManager(ManagerDAO man, string first, string last, string user, string pass);

        [OperationContract]
        void DeleteManager(ManagerDAO man);
        /*********************************************/

        [OperationContract]
        List<ProjectDAO> GetAllProjects();

        [OperationContract]
        ProjectDAO GetProject(int id);

        [OperationContract]
        ProjectDAO GetProject(string name);

        [OperationContract]
        List<ProjectDAO> GetProjectByManager(ManagerDAO man);

        [OperationContract]
        List<string> GetProjectStatus();

        [OperationContract]
        ProjectDAO MakeProject(string name, DateTime start, DateTime end, string status, ManagerDAO manager);

        [OperationContract]
        void DeleteProject(ProjectDAO proj);
        /*********************************************/

        [OperationContract]
        List<WorkerDAO> GetAllWorkers();

        [OperationContract]
        List<WorkerDAO> GetAllWorkers(ProjectDAO proj);

        [OperationContract]
        WorkerDAO GetWorker(int id);

        [OperationContract]
        WorkerDAO GetWorker(string first, string last);

        [OperationContract]
        List<WorkerDAO> GetWorkerByTitle(string title);

        [OperationContract]
        List<string> GetWorkTitles();
        
        [OperationContract]
         List<string> GetWorkStatus();
        
        [OperationContract]
        WorkerDAO MakeWorker(string first, string last, string title, ProjectDAO proj, string status);
        
        [OperationContract]
        WorkerDAO UpdateWorker(WorkerDAO work, string first, string last, string title, ProjectDAO proj, string status);

        [OperationContract]
        void DelWorker(WorkerDAO work);
        /****************************************/

        [OperationContract]
        List<MaterialDAO> GetAllMaterial();

        [OperationContract]
        List<MaterialDAO> GetAllMaterial(ProjectDAO proj);

        [OperationContract]
        List<MaterialDAO> GetMaterial(string name);

        [OperationContract]
        MaterialDAO AddMaterial(string name, int amntNeeded, int amntHave, ProjectDAO proj);

        [OperationContract]
        MaterialDAO EditMaterial(MaterialDAO mat, string name, int amntNeeded, int amntHave, ProjectDAO proj);

        [OperationContract]
        void DelMaterial(MaterialDAO mat);

    }
}

﻿using CMSDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ConstructService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CMSService" in both code and config file together.
    public class CMSService : ICMSService
    {
        private DataAccess da = new DataAccess();

        #region Manager
        /*********** Get **********/
        public List<ManagerDAO> GetAllManagers()
        {
            var l = da.GetAllManagers();
            var list = new List<ManagerDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public ManagerDAO GetManager(int id)
        {
            return Convert(da.GetManager(id));
        }

        public ManagerDAO GetManager(string first, string last)
        {
            return Convert(da.GetManager(first, last));
        }

        public ManagerDAO GetManagerByLogin(string user, string pass)
        {
            return Convert(da.GetManagerByLogin(user, pass));
        }

        public ManagerDAO MakeManager(string first, string last, string user, string pass)
        {
            return Convert(da.MakeManager(first, last, user, pass));
        }

        /******** Update **************/
        public ManagerDAO UpdateManager(ManagerDAO man, string first, string last, string user, string pass)
        {
            var x = da.UpdateManager(Convert(man), first, last, user, pass);
            return Convert(x);
        }

        /********* Delete **************/
        public void DeleteManager(ManagerDAO man)
        {
            da.DeleteManager(Convert(man));
        }
        #endregion

        #region Project
        public List<ProjectDAO> GetAllProjects()
        {
            var l = da.GetAllProjects();
            var list = new List<ProjectDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public ProjectDAO GetProject(int id)
        {
            return Convert(da.GetProject(id));
        }

        public ProjectDAO GetProject(string name)
        {
            return Convert(da.GetProject(name));
        }

        public List<ProjectDAO> GetProjectByManager(ManagerDAO man)
        {
            var l = da.GetProjectByManager(Convert(man));
            var list = new List<ProjectDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<string> GetProjectStatus()
        {
            var l = da.GetProjectStatus();
            var list = new List<string>();
            foreach (var x in l)
                list.Add(x.Status);

            return list;
        }

        public ProjectDAO MakeProject(string name, DateTime start, DateTime end, string status, ManagerDAO manager)
        {
            var s = da.GetProjectStatus().FirstOrDefault(i => i.Status == status);
            var x = da.MakeProject(name, start, end, s, Convert(manager));
            return Convert(x);
        }

        public ProjectDAO UpdateProject(ProjectDAO proj, string name, DateTime start, DateTime end, string status, ManagerDAO manager)
        {
            var s = da.GetProjectStatus().FirstOrDefault(i => i.Status == status);
            var x = da.UpdateProject(Convert(proj), name, start, end, s, Convert(manager));
            return Convert(x);
        }

        public void DeleteProject(ProjectDAO proj)
        {
            da.DeleteProject(Convert(proj));
        }
        #endregion

        #region Worker
        public List<WorkerDAO> GetAllWorkers()
        {
            var l = da.GetAllWorkers();
            var list = new List<WorkerDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<WorkerDAO> GetAllWorkers(ProjectDAO proj)
        {
            var l = da.GetAllWorkers(Convert(proj));
            var list = new List<WorkerDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public WorkerDAO GetWorker(int id)
        {
            return Convert(da.GetWorker(id));
        }

        public WorkerDAO GetWorker(string first, string last)
        {
            return Convert(da.GetWorker(first, last));
        }

        public List<WorkerDAO> GetWorkerByTitle(string title)
        {
            var t = da.GetWorkTitles().FirstOrDefault(i => i.TitleName == title);
            var l = da.GetWorkerByTitle(t);
            var list = new List<WorkerDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<string> GetWorkTitles()
        {
            var l = da.GetWorkTitles();
            var list = new List<string>();
            foreach (var x in l)
                list.Add(x.TitleName);

            return list;
        }

        public List<string> GetWorkStatus()
        {
            var l = da.GetWorkStatus();
            var list = new List<string>();
            foreach (var x in l)
                list.Add(x.Status);

            return list;
        }

        public WorkerDAO MakeWorker(string first, string last, string title, ProjectDAO proj, string status)
        {
            var t = da.GetWorkTitles().FirstOrDefault(i => i.TitleName == title);
            var s = da.GetWorkStatus().FirstOrDefault(i => i.Status == status);
            return Convert(da.MakeWorker(first, last, t, Convert(proj), s));
        }

        public WorkerDAO UpdateWorker(WorkerDAO work, string first, string last, string title, ProjectDAO proj, string status)
        {
            var t = da.GetWorkTitles().FirstOrDefault(i => i.TitleName == title);
            var s = da.GetWorkStatus().FirstOrDefault(i => i.Status == status);
            var x = da.UpdateWorker(Convert(work), first, last, t, Convert(proj), s);

            return Convert(x);
        }

        public void DelWorker(WorkerDAO work)
        {
            da.DelWorker(Convert(work));
        }
        #endregion

        #region Material
        public List<MaterialDAO> GetAllMaterial()
        {
            var l = da.GetAllMaterial();
            var list = new List<MaterialDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<MaterialDAO> GetAllMaterial(ProjectDAO proj)
        {
            var l = da.GetAllMaterial(Convert(proj));
            var list = new List<MaterialDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<MaterialDAO> GetMaterial(string name)
        {
            var l = da.GetMaterial(name);
            var list = new List<MaterialDAO>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public MaterialDAO AddMaterial(string name, int amntNeeded, int amntHave, ProjectDAO proj)
        {
            return Convert(da.AddMaterial(name, amntNeeded, amntHave, Convert(proj)));
        }

        public MaterialDAO EditMaterial(MaterialDAO mat, string name, int amntNeeded, int amntHave, ProjectDAO proj)
        {
            return Convert(da.EditMaterial(Convert(mat), name, amntNeeded, amntHave, Convert(proj)));
        }

        public void DelMaterial(MaterialDAO mat)
        {
            da.DelMaterial(Convert(mat));
        }


        #endregion

        #region Converter
        private ManagerDAO Convert(Person man)
        {
            return new ManagerDAO() { first = man.FirstName, last = man.LastName, id = man.ManagerID, pass = man.Pass, user = man.UserName };
        }

        private Person Convert(ManagerDAO man)
        {
            return new Person() { FirstName = man.first, LastName = man.last, ManagerID = man.id, Pass = man.pass, UserName = man.user };
        }

        private ProjectDAO Convert(Project proj)
        {
            var x = new ProjectDAO()
            {
                id = proj.ProjectID, end = (DateTime)proj.EndDate, start = proj.StartDate,
                manager = Convert(proj.Person), name = proj.ProjectName, status = proj.ProjectStatu.Status
            };
            return x;
        }

        private Project Convert(ProjectDAO proj)
        {
            Project x = new Project();
            x.ProjectID = proj.id; x.EndDate = proj.end; x.StartDate = proj.start;
            x.Person = Convert(proj.manager); x.ProjectName = proj.name;
            x.ProjectStatu = da.GetProjectStatus().FirstOrDefault(i => i.Status == proj.status);

            return x;
        }

        private WorkerDAO Convert(Worker work)
        {
            return new WorkerDAO() { first = work.FirstName, last = work.LastName, proj = Convert(work.Project), status = work.WorkStatu.Status, title = work.WorkTitle.TitleName, id = work.WorkerID };
        }

        private Worker Convert(WorkerDAO work)
        {
            var x = new Worker()
            {
                FirstName = work.first,
                LastName = work.last,
                ProjectID = work.proj.id,
                Project = Convert(work.proj),
                WorkerID = work.id
            };
            x.WorkStatu = da.GetWorkStatus().FirstOrDefault(i => i.Status == work.status);
            x.WorkTitle = da.GetWorkTitles().FirstOrDefault(i => i.TitleName == work.title);

            return x;
        }

        private MaterialDAO Convert(Material mat)
        {
            return new MaterialDAO()
            {
                name = mat.MaterialName, amntHave = mat.QuantityHave,
                amntNeed = mat.QuantityNeed, proj = Convert(mat.Project),
                id = mat.MaterialID
            };
        }

        private Material Convert(MaterialDAO mat)
        {
            return new Material()
            {
                MaterialName = mat.name,
                QuantityHave = mat.amntHave,
                QuantityNeed = mat.amntNeed,
                Project = Convert(mat.proj),
                ProjectID = mat.proj.id,
                MaterialID = mat.id
            };
        }
        #endregion
    }
}

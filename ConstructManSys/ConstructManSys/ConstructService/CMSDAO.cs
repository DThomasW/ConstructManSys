﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructService
{
    public class ManagerDAO
    {
        public int id { get; set; }
        public string first { get; set; }
        public string last { get; set; }
        public string user { get; set; }
        public string pass { get; set; }
    }

    public class ProjectDAO
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string status { get; set; }
        public ManagerDAO manager { get; set; }
    }

    public class WorkerDAO
    {
        public int id { get; set; }
        public string first { get; set; }
        public string last { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public ProjectDAO proj { get; set; }
    }

    public class MaterialDAO
    {
        public int id { get; set; }
        public string name { get; set; }
        public int amntNeed { get; set; }
        public int amntHave { get; set; }
        public ProjectDAO proj { get; set; }
    }
}

﻿using ConstructService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConManSysClient.Models
{
    public class ServiceLink
    {
        public CMSService ser = new CMSService();

        #region Manager
        /*********** Get **********/
        public List<ManagerMod> GetAllManagers()
        {
            var l = ser.GetAllManagers();
            var list = new List<ManagerMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public ManagerMod GetManager(int id)
        {
            return Convert(ser.GetManager(id));
        }

        public ManagerMod GetManager(string first, string last)
        {
            return Convert(ser.GetManager(first, last));
        }

        public ManagerMod GetManagerByLogin(string user, string pass)
        {
            return Convert(ser.GetManagerByLogin(user, pass));
        }

        public ManagerMod MakeManager(string first, string last, string user, string pass)
        {
            return Convert(ser.MakeManager(first, last, user, pass));
        }

        /******** Update **************/
        public ManagerMod UpdateManager(ManagerMod man, string first, string last, string user, string pass)
        {
            var x = ser.UpdateManager(Convert(man), first, last, user, pass);
            return Convert(x);
        }

        /********* Delete **************/
        public void DeleteManager(ManagerMod man)
        {
            ser.DeleteManager(Convert(man));
        }
        #endregion

        #region Project
        public List<ProjectMod> GetAllProjects()
        {
            var l = ser.GetAllProjects();
            var list = new List<ProjectMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public ProjectMod GetProject(int id)
        {
            return Convert(ser.GetProject(id));
        }

        public ProjectMod GetProject(string name)
        {
            return Convert(ser.GetProject(name));
        }

        public List<ProjectMod> GetProjectByManager(ManagerMod man)
        {
            var l = ser.GetProjectByManager(Convert(man));
            var list = new List<ProjectMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<string> GetProjectStatus()
        {
            return ser.GetProjectStatus();
        }

        public ProjectMod MakeProject(string name, DateTime start, DateTime end, string status, ManagerMod manager)
        {
            var x = ser.MakeProject(name, start, end, status, Convert(manager));
            return Convert(x);
        }

        public ProjectMod UpdateProject(ProjectMod proj, string name, DateTime start, DateTime end, string status, ManagerMod manager)
        {
            var x = ser.UpdateProject(Convert(proj), name, start, end, status, Convert(manager));
            return Convert(x);
        }

        public void DeleteProject(ProjectMod proj)
        {
            ser.DeleteProject(Convert(proj));
        }
        #endregion

        #region Worker
        public List<WorkerMod> GetAllWorkers()
        {
            var l = ser.GetAllWorkers();
            var list = new List<WorkerMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<WorkerMod> GetAllWorkers(ProjectMod proj)
        {
            var l = ser.GetAllWorkers(Convert(proj));
            var list = new List<WorkerMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public WorkerMod GetWorker(int id)
        {
            return Convert(ser.GetWorker(id));
        }

        public WorkerMod GetWorker(string first, string last)
        {
            return Convert(ser.GetWorker(first, last));
        }

        public List<WorkerMod> GetWorkerByTitle(string title)
        {
            var l = ser.GetWorkerByTitle(title);
            var list = new List<WorkerMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<string> GetWorkTitles()
        {
            return ser.GetWorkTitles();
        }

        public List<string> GetWorkStatus()
        {
            return ser.GetWorkStatus();
        }

        public WorkerMod MakeWorker(string first, string last, string title, ProjectMod proj, string status)
        {
            return Convert(ser.MakeWorker(first, last, title, Convert(proj), status));
        }

        public WorkerMod UpdateWorker(WorkerMod work, string first, string last, string title, ProjectMod proj, string status)
        {
            var x = ser.UpdateWorker(Convert(work), first, last, title, Convert(proj), status);

            return Convert(x);
        }

        public void DelWorker(WorkerMod work)
        {
            ser.DelWorker(Convert(work));
        }
        #endregion

        #region Material
        public List<MaterialMod> GetAllMaterial()
        {
            var l = ser.GetAllMaterial();
            var list = new List<MaterialMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<MaterialMod> GetAllMaterial(ProjectMod proj)
        {
            var l = ser.GetAllMaterial(Convert(proj));
            var list = new List<MaterialMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public List<MaterialMod> GetMaterial(string name)
        {
            var l = ser.GetMaterial(name);
            var list = new List<MaterialMod>();
            foreach (var x in l)
                list.Add(Convert(x));

            return list;
        }

        public MaterialMod AddMaterial(string name, int amntNeeded, int amntHave, ProjectMod proj)
        {
            return Convert(ser.AddMaterial(name, amntNeeded, amntHave, Convert(proj)));
        }

        public MaterialMod EditMaterial(MaterialMod mat, string name, int amntNeeded, int amntHave, ProjectMod proj)
        {
            return Convert(ser.EditMaterial(Convert(mat), name, amntNeeded, amntHave, Convert(proj)));
        }

        public void DelMaterial(MaterialMod mat)
        {
            ser.DelMaterial(Convert(mat));
        }


        #endregion

        #region Converter
        private ManagerMod Convert(ManagerDAO man)
        {
            return new ManagerMod() {FirstName = man.first, LastName = man.last, managerID = man.id, Password = man.pass, UserName = man.user };
        }

        private ManagerDAO Convert(ManagerMod man)
        {
            return new ManagerDAO() { first = man.FirstName, last = man.LastName, user = man.UserName, pass = man.Password, id = man.managerID };
        }

        private ProjectMod Convert(ProjectDAO proj)
        {
            var x = new ProjectMod()
            {
                name = proj.name,
                id = proj.id,
                start = proj.start,
                end = proj.end,
                manager = Convert(proj.manager),
                status = proj.status
            };
            return x;
        }

        private ProjectDAO Convert(ProjectMod proj)
        {
            ProjectDAO x = new ProjectDAO();
            x.id = proj.id; x.end = proj.end; x.start = proj.start;
            x.manager = Convert(proj.manager); x.name = proj.name;
            x.status = proj.status;

            return x;
        }

        private WorkerMod Convert(WorkerDAO work)
        {
            return new WorkerMod() { first = work.first, last = work.last, proj = Convert(work.proj), status = work.status, title = work.title, workerID = work.id };
        }

        private WorkerDAO Convert(WorkerMod work)
        {
            var x = new WorkerDAO()
            {
                first = work.first,
                last = work.last,
                proj = Convert(work.proj),
                id = work.workerID
            };
            x.status = work.status;
            x.title = work.title;

            return x;
        }

        private MaterialMod Convert(MaterialDAO mat)
        {
            return new MaterialMod()
            {
                name = mat.name,
                amntHave = mat.amntHave,
                amntNeed = mat.amntNeed,
                proj = Convert(mat.proj),
                materialID = mat.id
            };
        }

        private MaterialDAO Convert(MaterialMod mat)
        {
            return new MaterialDAO()
            {
                name = mat.name,
                amntHave = mat.amntHave,
                amntNeed = mat.amntNeed,
                proj = Convert(mat.proj),
                id = mat.materialID
            };
        }
        #endregion
    }
}

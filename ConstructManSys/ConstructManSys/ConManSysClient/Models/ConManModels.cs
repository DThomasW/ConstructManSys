﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ConManSysClient.Models
{
    public class ManagerMod
    {
        public int managerID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        [Required(ErrorMessage ="First Name is Required")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is Required")]
        public string LastName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "UserName")]
        [Required(ErrorMessage = "Username is Required")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Password is Required")]
        public string Password { get; set; }
    }

    public class ProjectMod
    {
        public int id { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Project Name")]
        [Required(ErrorMessage = "Project needs a name")]
        public string name { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        [Required(ErrorMessage = "Need a start date")]
        public DateTime start { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        public DateTime end { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Project Status")]
        public string status { get; set; }

        public ManagerMod manager { get; set; }
        public List<SelectListItem> statusItem { get; set; }

        public List<WorkerMod> workers { get; set; }
        public List<MaterialMod> mat { get; set; }

        public ProjectMod()
        {
            name = "";
            status = "";
            statusItem = new List<SelectListItem>();
        }
    }

    public class newProj
    {
        public ProjectMod nProj { get; set; }
        public ManagerMod man { get; set; }
        public int manID { get; set; }

        public newProj()
        {
            nProj = new ProjectMod();
            man = new ManagerMod();
        }
    }

    public class newWorker
    {
        public ProjectMod nProj { get; set; }
        public WorkerMod work { get; set; }

        public newWorker()
        {
            nProj = new ProjectMod();
            work = new WorkerMod();
        }
    }

    public class newMaterial
    {
        public ProjectMod nProj { get; set; }
        public MaterialMod mat { get; set; }

        public newMaterial()
        {
            nProj = new ProjectMod();
            mat = new MaterialMod();
        }
    }

    public class ProjList
    {
        public IEnumerable<ProjectMod> projList { get; set; }
        public ManagerMod man { get; set; }
        
        public ProjList()
        {
            man = new ManagerMod();
        }
    }

    public class WorkList
    {
        public IEnumerable<WorkerMod> workList { get; set; }
        public ProjectMod proj { get; set; }
        public WorkList()
        {
            proj = new ProjectMod();
        }
    }

    public class MatList
    {
        public IEnumerable<MaterialMod> matList { get; set; }
        public ProjectMod proj { get; set; }
        public MatList()
        {
            proj = new ProjectMod();
        }
    }

    public class WorkerMod
    {
        public int workerID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name is Required")]
        public string first { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is Required")]
        public string last { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Job Title")]
        public string title { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Worker Status")]
        public string status { get; set; }

        public ProjectMod proj { get; set; }
        public int projID { get; set; }
        public List<SelectListItem> statusItem { get; set; }
        public List<SelectListItem> titleItem { get; set; }

        public WorkerMod()
        {
            first = "";
            last = "";
            title = "";
            status = "";
            statusItem = new List<SelectListItem>();
            titleItem = new List<SelectListItem>();
        }
    }

    public class MaterialMod
    {
        public int materialID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Material Name")]
        public string name { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Current Quantity")]
        public int amntHave { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Quantity Needed")]
        public int amntNeed { get; set; }

        public ProjectMod proj { get; set; }
        public int projID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

namespace ConManSysClient.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundle/graph-scripts")
                .Include("~/Scripts/d3/d3.js")
                .Include("~/Scripts/d3/d3.min.js")
                .Include("~/Scripts/CMSgraphs.js"));
        }
    }
}

﻿using ConManSysClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConManSysClient.Controllers
{
    public class ManagerController : Controller
    {

        private ServiceLink ser = new ServiceLink();

        public ActionResult Index(int id)
        {
            return RedirectToAction("Index", "Project", new { id = id });
        }

        // GET: Manager
        public ActionResult ProjectList(int id)
        {
            var man = ser.GetManager(id);
            ProjList list = new ProjList();
            list.projList = ser.GetProjectByManager(man);
            list.man = man;
            return View(list);
        }

        // GET: Manager/Create
        public ActionResult CreateProject(int id)
        {
            ProjectMod proj = new ProjectMod();
            var stat = ser.GetProjectStatus();
            foreach (var x in stat)
            {
                proj.statusItem.Add(new SelectListItem()
                {
                    Text = x,
                    Value = x
                });
            }
            return View(new newProj() { manID = id, nProj = proj });
        }

        // POST: Manager/Create
        [HttpPost]
        public ActionResult CreateProject(newProj proj)
        {
            try
            {
                var man = ser.GetManager(proj.manID);
                var nProj = ser.MakeProject(proj.nProj.name, proj.nProj.start, proj.nProj.end, proj.nProj.status, man);
                if (nProj != null)
                    return RedirectToAction("ProjectList", new { id = man.managerID });


                throw new Exception();
            }
            catch
            {
                return RedirectToAction("CreateProject", new { id = proj.man.managerID });
            }
        }

        public ActionResult EditManager(int id)
        {
            var man = ser.GetManager(id);
            return View(man);
        }

        [HttpPost]
        public ActionResult EditManager(ManagerMod man)
        {
            var temp = ser.GetManager(man.managerID);
            man = ser.UpdateManager(temp, man.FirstName, man.LastName, man.UserName, man.Password);
            return RedirectToAction("ProjectList", new { id = man.managerID });
        }

        public ActionResult EditProject(int id)
        {
            ProjectMod nProj = ser.GetProject(id);
            var stat = ser.GetProjectStatus();
            foreach (var x in stat)
            {
                nProj.statusItem.Add(new SelectListItem()
                {
                    Text = x,
                    Value = x
                });
            }
            return View(nProj);
        }

        [HttpPost]
        public ActionResult EditProject(ProjectMod proj)
        {
            var p = ser.GetProject(proj.id);
            var temp = ser.UpdateProject(p, proj.name, proj.start, proj.end, proj.status, p.manager);

            return RedirectToAction("ProjectList", new { id = p.manager.managerID });
        }

        // GET: Manager/Delete/5
        public ActionResult DelProject(int id)
        {
            var proj = ser.GetProject(id);
            return View(proj);
        }

        [HttpPost, ActionName("DelProject")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                var p = ser.GetProject(id);
                var man = ser.GetManager(p.manager.managerID);
                ser.DeleteProject(p);
                return RedirectToAction("ProjectList", new { id = man.managerID });
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using ConManSysClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConManSysClient.Controllers
{
    public class HomeController : Controller
    {

        private ServiceLink ser = new ServiceLink();

        // GET: Home
        public ActionResult Login()
        {
            return View();
        }

        // GET: Home/Details/5
        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp([Bind(Include = "FirstName, LastName, Username, Password")] ManagerMod man)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    var log = ser.MakeManager(man.FirstName, man.LastName, man.UserName, man.Password);
                    if (log != null)
                        return RedirectToAction("ProjectList", "Manager", new { id = log.managerID });
                    else
                    {
                        
                        return RedirectToAction("SignUp");
                    }
                }
                throw new Exception();
            }
            catch
            {
                return View();
            }
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Login([Bind(Include ="Username, Password")] ManagerMod man)
        {
            try
            {
               var log = ser.GetManagerByLogin(man.UserName, man.Password);
               if (log == null)
                   return RedirectToAction("Login");

               return RedirectToAction("ProjectList", "Manager", new { id = log.managerID });
                
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using ConManSysClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConManSysClient.Controllers
{
    public class ProjectController : Controller
    {
        private ServiceLink ser = new ServiceLink();

        // GET: Project
        public ActionResult Index(int id)
        {
            var p = ser.GetProject(id);
            p.mat = ser.GetAllMaterial(p);
            p.workers = ser.GetAllWorkers(p);

            return View(p);
        }

        // GET: Project/Details/5
        public ActionResult WorkerList(int id)
        {
            var p = ser.GetProject(id);
            var l = ser.GetAllWorkers(p);
            var list = new WorkList();
            list.proj = p;
            list.workList = l;
            return View(list);
        }

        public ActionResult MaterialList(int id)
        {
            var p = ser.GetProject(id);
            var l = ser.GetAllMaterial(p);
            var list = new MatList();
            list.proj = p;
            list.matList = l;
            return View(list);
        }

        public ActionResult Return(int id)
        {
            return RedirectToAction("ProjectList", "Manager", new { id = id });
        }

        public ActionResult HireEmployee(int id)
        {
            var p = ser.GetProject(id);
            var work = new WorkerMod();
            work.proj = p; work.projID = p.id;

            var stat = ser.GetWorkStatus();
            var title = ser.GetWorkTitles();
            foreach (var x in stat)
                work.statusItem.Add(new SelectListItem() { Text = x, Value = x });
            foreach (var x in title)
                work.titleItem.Add(new SelectListItem() { Text = x, Value = x });

            return View(work);
        }

        public ActionResult AddMaterial(int id)
        {
            var p = ser.GetProject(id);
            var nMat = new MaterialMod();
            nMat.proj = p;
            nMat.projID = p.id;

            return View(nMat);
        }

        public ActionResult EditWorker(int id)
        {
            var work = ser.GetWorker(id);
            work.projID = work.proj.id;
            var stat = ser.GetWorkStatus();
            var t = ser.GetWorkTitles();

            foreach (var x in stat)
                work.statusItem.Add(new SelectListItem() { Text = x, Value = x });
            foreach (var x in t)
                work.titleItem.Add(new SelectListItem() { Text = x, Value = x });

            return View(work);
        }

        public ActionResult EditMaterial(int id)
        {
            var mat = ser.GetAllMaterial().FirstOrDefault(x => x.materialID == id);
            mat.projID = mat.proj.id;

            return View(mat);
        }

        // POST: Project/Create
        [HttpPost]
        public ActionResult HireEmployee(WorkerMod work)
        {
            try
            {
                var proj = ser.GetProject(work.projID);
                work = ser.MakeWorker(work.first, work.last, work.title, proj, work.status);

                if (work != null)
                    return RedirectToAction("Index", new { id = proj.id });

                throw new Exception();
            }
            catch
            {
                return RedirectToAction("Index", new { id = work.projID });
            }
        }

        [HttpPost]
        public ActionResult AddMaterial(MaterialMod mat)
        {
            try
            {
                var nProj = ser.GetProject(mat.projID);
                var nMat = ser.AddMaterial(mat.name, mat.amntNeed, mat.amntHave, nProj);

                if (nMat != null)
                    return RedirectToAction("Index", new { id = mat.projID });

                throw new Exception();
            }
            catch
            {
                return View(mat);
            }
        }

        [HttpPost]
        public ActionResult EditWorker(WorkerMod work)
        {
            try
            {
                var proj = ser.GetProject(work.projID);
                var w = ser.GetWorker(work.workerID);
                work = ser.UpdateWorker(w, work.first, work.last, work.title, proj, work.status);

                if (work != null)
                    return RedirectToAction("Index", new { id = proj.id });

                throw new Exception();
            }
            catch
            {
                return RedirectToAction("EditWorker", new { id = work.projID });
            }
        }

        [HttpPost]
        public ActionResult EditMaterial(MaterialMod mat)
        {
            try
            {
                var nProj = ser.GetProject(mat.projID);
                var m = ser.GetAllMaterial().FirstOrDefault(x => x.materialID == mat.materialID);
                var nMat = ser.EditMaterial(m, mat.name, mat.amntNeed, mat.amntHave, nProj);

                if (nMat != null)
                    return RedirectToAction("Index", new { id = mat.projID });

                throw new Exception();
            }
            catch
            {
                return View(mat);
            }
        }

        public ActionResult DelMaterial(int id)
        {
            var mat = ser.GetAllMaterial().FirstOrDefault(x => x.materialID == id);
            var proj = mat.proj;

            ser.DelMaterial(mat);

            return RedirectToAction("Index", new { id = proj.id });
        }

        public ActionResult DelWorker(int id)
        {
            var work = ser.GetWorker(id);
            var proj = work.proj;

            ser.DelWorker(work);

            return RedirectToAction("Index", new { id = proj.id });
        }
    }


}

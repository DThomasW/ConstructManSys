﻿(function (app, q) {
    'use strict';
    var projName = JSON.parse(document.getElementById("projName").innerHTML);
    var projStat = JSON.parse(document.getElementById("projStatus").innerHTML);
    var workList = JSON.parse(document.getElementById("workList").innerHTML);
    var matList = JSON.parse(document.getElementById("matList").innerHTML);
    var man = JSON.parse(document.getElementById("man").innerHTML);

    var listSize = matList.length + workList.length;

    var diameter = 350 + (listSize*20),
        format = d3.format(",d");
    var pack = d3.layout.pack()
        .size([diameter, diameter])
        .value(function (d) { return d.size; });

    var svg = d3.select("#graphSec").append("svg")
        .attr("width", diameter+75)
        .attr("height", diameter+75)
        .append("g")
        .attr("transform", "translate(2,2)");

    var node = svg.datum(projName).selectAll(".node")
        .data(pack.nodes)
        .enter().append("g")
        .attr("class", "project")
        .attr("transform", function (d) { return "translate(" + (diameter * 0.5) + "," + (diameter * 0.5) + ")"; });

    node.append("title")
        .text(function (i) {
            var s = projName + "--> Status: " + projStat + " -- Manager: " + man.FirstName + " " + man.LastName;
            return s;
        });
    node.append("circle")
        .attr("r", function (i) { return diameter * 0.50 });
    node.append("text")
        .attr("dy", ".5em")
        .style("text-anchor", "middle")
        .text(projName)
        .style("pointer-events", "none");

    var radius = diameter / (16 + (workList.length - 1));
    var offset = (diameter * 0.305);

    offset += ((diameter * 0.0125) * parseInt(workList.length / 9));

    node.append("circle")
        .attr("r", function (i) {
            var r = diameter * 0.25;
            return r;
        })
        .attr("class", "project")
        .attr("transform", function (d) { return "translate(" + -(diameter * 0.175) + "," + -(diameter * 0.175) + ")"; })
        .append("title")
        .text("Workers");

    var id = 0;
    var add = 1;

    var workNode = svg.datum(workList).selectAll(".workNode")
        .data(workList)
        .enter().append("g")
        .attr("class", "work")
        .attr("transform", function (d) {

            if (id >= 9) {
                add += parseInt(id / 9);
                id = id % 9;
            }

            var offX = add;
            var offY = add;
            if (id == 0) {
                offX = 0;
                offY = 0;
            }
            else if (id % 8 == 0) {
                offY = -offY;
                offX = -offX;
            }
            else if (id % 7 == 0) {
                offY = -offY;
                offX = 0;
            }
            else if (id % 6 == 0) {
                offY = -offY;
            }
            else if (id % 5 == 0) {
                offY = 0;
            }
            else if (id % 4 == 0) {
                offX = offX;
                offY = offY;
            }
            else if (id % 3 == 0) {
                offX = 0;
            }
            else if (id % 2 == 0) {
                offX = -offX;
            }
            else if (id % 1 == 0) {
                offY = 0;
                offX = -offX;
            }


            offY = offset + ((radius * 2) * offY);
            offX = offset + ((radius * 2) * offX);
            id += 1;

            return "translate(" + offX + "," + offY + ")";
        })
        .append("a")
        .attr("xlink:href", function (d) {
            var s = "../EditWorker/" + d.workerID;
            return s;
        });

    workNode.append("title")
        .text(function (d) {
            var s = d.first + " " + d.last + "--> Title: " + d.title + " -- Status: " + d.status;
            return s;
        });
    workNode.append("circle")
        .attr("r", function (d) { return radius; });
    workNode.append("text")
        .attr("dy", ".3em")
        .style("text-anchor", "middle")
        .text(function (d) { return d.last.substring(0, radius / 3); })
        .style("pointer-events", "none");


    /*******************************/

    var rad = diameter / (14 + (matList.length - 1));
    var off = (diameter * 0.685);
    off += (diameter*0.0125) * parseInt(matList.length/9);
    var size = 0;

    node.append("circle")
       .attr("r", function (i) {
           var r = diameter * 0.25;
           return r;
       })
       .attr("class", "project")
       .attr("transform", function (d) { return "translate(" + ((diameter * 0.175)) + "," + ((diameter * 0.175)) + ")"; })
        .append("title")
        .text("Material");

    var id = 0;
    var add = 1;

    for (var x = 0; x < matList.length; x++) {
        size += matList[x].amntHave;
    }

    var matNode = svg.datum(matList).selectAll(".matNode")
        .data(matList)
        .enter().append("g")
        .attr("class", "mat")
        .attr("transform", function (d) {

            if (id >= 9) {
                add += parseInt(id / 9);
                id = id % 9;
            }

            var offX = add;
            var offY = add;

            if (id == 0) {
                offX = 0;
                offY = 0;
            }
            else if (id % 8 == 0) {
                offY = -offY;
                offX = -offX;
            }
            else if (id % 7 == 0) {
                offY = -offY;
                offX = 0;
            }
            else if (id % 6 == 0) {
                offY = -offY;
            }
            else if (id % 5 == 0) {
                offY = 0;
            }
            else if (id % 4 == 0) {
                offX = offX;
                offY = offY;
            }
            else if (id % 3 == 0) {
                offX = 0;
            }
            else if (id % 2 == 0) {
                offX = -offX;
            }
            else if (id % 1 == 0) {
                offY = 0;
                offX = -offX;
            }

            var adjRad = d.amntHave / (size * 0.55);
            if (adjRad > 1)
                adjRad = 1;
            else if (adjRad < 0.5)
                adjRad = 0.5;

            offY = off + ((rad * 2 * adjRad) * offY);
            offX = off + ((rad * 2 * adjRad) * offX);


            id += 1;
            return "translate(" + offX + "," + offY + ")";
        })
        .append("a")
        .attr("xlink:href", function (d) {
            var s = "../EditMaterial/" + d.materialID;
            return s;
        });

    matNode.append("title")
       .text(function (d) {
           var s = d.name + "-> Have: " + d.amntHave + " -- Need: " + d.amntNeed;
           return s;
       });
    matNode.append("circle")
        .attr("r", function (d) {
            var adjRad = d.amntHave / (size * 0.55);
            if (adjRad > 1)
                adjRad = 1;
            else if (adjRad < 0.5)
                adjRad = 0.5;
            return rad *adjRad;
        });
    matNode.append("text")
        .attr("dy", ".3em")
        .style("text-anchor", "middle")
        .text(function (d) {
            var s = d.name;
            var adjRad = d.amntHave / (size * 0.55);
            if (adjRad > 1)
                adjRad = 1;
            else if (adjRad < 0.5)
                adjRad = 0.5;
            var temp = (rad * adjRad) / 3;
            if (temp < 1) temp = 1;

            return s.substring(0, temp);
        })
        .style("pointer-events", "none");

    /******************************************/

    d3.select(self.frameElement).style("height", diameter + "px");

})(window.ConstructManSys = window.ConstructManSys || {}, jQuery);
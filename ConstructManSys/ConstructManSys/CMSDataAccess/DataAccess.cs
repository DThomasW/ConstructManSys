﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSDataAccess
{
    public class DataAccess
    {
        private CMSDBEntities conSys = new CMSDBEntities();

        #region Manager
        /*********** Get **********/
        public List<Person> GetAllManagers()
        {
            return conSys.People.ToList();
        }

        public Person GetManager(int id)
        {
            return conSys.People.FirstOrDefault(x => x.ManagerID == id);
        }

        public Person GetManager(string first, string last)
        {
            return conSys.People.FirstOrDefault(x => x.FirstName == first && x.LastName == last);
        }

        public Person GetManagerByLogin(string user, string pass)
        {
            var p = conSys.People.FirstOrDefault(x => x.UserName == user && x.Pass == pass);
            return p;
        }

        /*********** Set ************/
        public Person MakeManager(string first, string last, string user, string pass)
        {
            try
            {

                if (conSys.People.FirstOrDefault(x => x.UserName == user) == null)
                {
                    Person p = new Person();
                    p.FirstName = first; p.LastName = last;
                    p.UserName = user; p.Pass = pass;

                    conSys.People.Add(p);
                    conSys.SaveChanges();

                    //conSys.sp_addManager(first, last, user, pass);

                    return p;
                }
                return null;
            }
            catch (DbEntityValidationException ex)
            {
                foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
                {
                    DbEntityEntry entry = item.Entry;
                    string enTypeName = entry.Entity.GetType().Name;

                    foreach (DbValidationError subitem in item.ValidationErrors)
                    {
                        string mess = string.Format("Error '{0}' occurred in {1} at {2}", subitem.ErrorMessage, enTypeName, subitem.PropertyName);

                    }

                }

                return null;
            }
            catch (DbUpdateException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /******** Update **************/
        public Person UpdateManager(Person man, string first, string last, string user, string pass)
        {
            conSys.sp_updateManager(man.ManagerID, first, last, user, pass);
            conSys.SaveChanges();
            return man;
        }

        /********* Delete **************/
        public void DeleteManager(Person man)
        {
            var x = conSys.People.FirstOrDefault(i => i.ManagerID == man.ManagerID);
            conSys.People.Remove(x);
            conSys.SaveChanges();
        }
        #endregion

        #region Project
        /*********** Get ****************/
        public List<Project> GetAllProjects()
        {
            return conSys.Projects.ToList();
        }

        public Project GetProject(int id)
        {
            return conSys.Projects.FirstOrDefault(x => x.ProjectID == id);
        }

        public Project GetProject(string name)
        {
            return conSys.Projects.FirstOrDefault(x => x.ProjectName == name);
        }

        public List<Project> GetProjectByManager(Person man)
        {
            List<Project> list = new List<Project>();
            foreach(var x in conSys.Projects)
            {
                if (x.ManagerID == man.ManagerID)
                    list.Add(x);
            }

            return list;
        }

        public List<ProjectStatu> GetProjectStatus()
        {
            return conSys.ProjectStatus.ToList();
        }

        /********** Set ******************/
        public Project MakeProject(string name, DateTime start, DateTime end, ProjectStatu status, Person manager)
        {
            Project proj = new Project();
            proj.ProjectName = name; proj.StartDate = start;
            proj.EndDate = end; proj.StatusID = status.StatusID;
            proj.ManagerID = manager.ManagerID;

            conSys.Projects.Add(proj);
            conSys.SaveChanges();

            return proj;
        }

        public Project UpdateProject(Project proj, string name, DateTime start, DateTime end, ProjectStatu status, Person manager)
        {
            conSys.sp_updateProject(proj.ProjectID, name, start, end, status.StatusID, manager.ManagerID);
            conSys.SaveChanges();

            return proj;
        }

        public void DeleteProject(Project proj)
        {
            var x = conSys.Projects.FirstOrDefault(i => i.ProjectID == proj.ProjectID);
            conSys.Projects.Remove(x);
            conSys.SaveChanges();
        }
        #endregion

        #region Worker
        /********* Workers **************/
        public List<Worker> GetAllWorkers()
        {
            return conSys.Workers.ToList();
        }

        public List<Worker> GetAllWorkers(Project proj)
        {
            List<Worker> list = new List<Worker>();
            foreach(var x in conSys.Workers)
            {
                if (x.ProjectID == proj.ProjectID)
                    list.Add(x);
            }

            return list;
        }

        public Worker GetWorker(int id)
        {
            return conSys.Workers.FirstOrDefault(x => x.WorkerID == id);
        }

        public Worker GetWorker(string first, string last)
        {
            return conSys.Workers.FirstOrDefault(x => x.FirstName == first && x.LastName == last);
        }

        public List<Worker> GetWorkerByTitle(WorkTitle title)
        {
            List<Worker> list = new List<Worker>();
            foreach (var x in conSys.Workers)
            {
                if (x.TitleID == title.TitleID)
                    list.Add(x);
            }

            return list;
        }

        public List<WorkTitle> GetWorkTitles()
        {
            return conSys.WorkTitles.ToList();
        }

        public List<WorkStatu> GetWorkStatus()
        {
            return conSys.WorkStatus.ToList();
        }

        /****** Set ***********/
        public Worker MakeWorker(string first, string last, WorkTitle title, Project proj, WorkStatu status)
        {
            Worker work = new Worker();
            work.FirstName = first; work.LastName = last;
            work.TitleID = title.TitleID;
            work.ProjectID = proj.ProjectID;
            work.StatusID = status.StatusID;

            conSys.Workers.Add(work);
            conSys.SaveChanges();

            return work;
        }

        public Worker UpdateWorker(Worker work, string first, string last, WorkTitle title, Project proj, WorkStatu status)
        {
            conSys.sp_updateWorker(work.WorkerID, first, last, status.StatusID, title.TitleID, proj.ProjectID);
            conSys.SaveChanges();

            return work;
        }

        public void DelWorker(Worker work)
        {
            var x = conSys.Workers.FirstOrDefault(i => i.WorkerID == work.WorkerID);
            conSys.Workers.Remove(x);
            conSys.SaveChanges();
        }
        #endregion

        #region Material
        /********* Material *********/
        public List<Material> GetAllMaterial()
        {
            return conSys.Materials.ToList();
        }

        public List<Material> GetAllMaterial(Project proj)
        {
            List<Material> list = new List<Material>();
            foreach (var x in conSys.Materials)
            {
                if (x.ProjectID == proj.ProjectID)
                    list.Add(x);
            }

            return list;
        }

        public List<Material> GetMaterial(string name)
        {
            List<Material> list = new List<Material>();
            foreach (var x in conSys.Materials)
            {
                if (x.MaterialName == name)
                    list.Add(x);
            }

            return list;
        }

        /**** Set *******/
        public Material AddMaterial(string name, int amntNeeded, int amntHave, Project proj)
        {
            Material mat = new Material();
            mat.MaterialName = name; mat.QuantityNeed = amntNeeded;
            mat.QuantityHave = amntHave;
            mat.ProjectID = proj.ProjectID;

            conSys.Materials.Add(mat);
            conSys.SaveChanges();

            return mat;
        }

        public Material EditMaterial(Material mat, string name, int amntNeeded, int amntHave, Project proj)
        {
            conSys.sp_updateMaterial(mat.MaterialID, name, amntNeeded, amntHave, proj.ProjectID);
            conSys.SaveChanges();

            return mat;
        }

        public void DelMaterial(Material mat)
        {
            var x = conSys.Materials.FirstOrDefault(i => i.MaterialID == mat.MaterialID);
            conSys.Materials.Remove(x);
            conSys.SaveChanges();
        }


        #endregion
    }
}
